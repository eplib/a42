<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A42172">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The speech made to Sir John Greenvile by Sir Harbot. Grimstone, Knight, speaker to the Honourable House of Commons, May 3, 1660.</title>
    <author>Grimston, Harbottle, Sir, 1603-1685.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A42172 of text R12047 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing G2039A). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A42172</idno>
    <idno type="STC">Wing G2039A</idno>
    <idno type="STC">ESTC R12047</idno>
    <idno type="EEBO-CITATION">11824942</idno>
    <idno type="OCLC">ocm 11824942</idno>
    <idno type="VID">49652</idno>
    <idno type="PROQUESTGOID">2264193889</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A42172)</note>
    <note>Transcribed from: (Early English Books Online ; image set 49652)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 31:19)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The speech made to Sir John Greenvile by Sir Harbot. Grimstone, Knight, speaker to the Honourable House of Commons, May 3, 1660.</title>
      <author>Grimston, Harbottle, Sir, 1603-1685.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by H.B. ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>[1660]</date>
     </publicationStmt>
     <notesStmt>
      <note>Date of publication from Wing.</note>
      <note>Thanking Grenville as bearer of the King's letter announcing his return.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Bath, John Grenville, -- Earl of, 1628-1701.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
     <term>Great Britain -- History -- Restoration, 1660-1688.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A42172</ep:tcp>
    <ep:estc> R12047</ep:estc>
    <ep:stc> (Wing G2039A). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>The speech made to Sir John Greenvile, by Sir Harbot. Grimstone Knight, Speaker to the Honourable House of Commons, May 3. 1660.</ep:title>
    <ep:author>Grimston, Harbottle, Sir</ep:author>
    <ep:publicationYear>1660</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>269</ep:wordCount>
    <ep:defectiveTokenCount>1</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>37</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 37 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-05</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-06</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2009-01</date>
    <label>Scott Lepisto</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-01</date>
    <label>Scott Lepisto</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A42172-e10">
  <body xml:id="A42172-e20">
   <pb facs="tcp:49652:1" rend="simple:additions" xml:id="A42172-001-a"/>
   <div type="speech" xml:id="A42172-e30">
    <head xml:id="A42172-e40">
     <w lemma="the" pos="d" xml:id="A42172-001-a-0010">The</w>
     <w lemma="speech" pos="n1" xml:id="A42172-001-a-0020">Speech</w>
     <w lemma="make" pos="vvn" xml:id="A42172-001-a-0030">made</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-0040">TO</w>
     <w lemma="sir" pos="n1" xml:id="A42172-001-a-0050">Sir</w>
     <w lemma="JOHN" pos="nn1" xml:id="A42172-001-a-0060">JOHN</w>
     <w lemma="Greenvile" pos="nn1" xml:id="A42172-001-a-0070">GREENVILE</w>
     <pc unit="sentence" xml:id="A42172-001-a-0080">.</pc>
    </head>
    <byline xml:id="A42172-e50">
     <w lemma="by" pos="acp" xml:id="A42172-001-a-0090">BY</w>
     <w lemma="sir" orig="Sᴵᴿ" pos="n1" xml:id="A42172-001-a-0100">SIR</w>
     <w lemma="HARBOT" pos="nn1" xml:id="A42172-001-a-0120">HARBOT</w>
     <w lemma="grimstone" pos="nn1" xml:id="A42172-001-a-0130">GRIMSTONE</w>
     <w lemma="knight" pos="n1" xml:id="A42172-001-a-0140">KNIGHT</w>
     <pc xml:id="A42172-001-a-0150">,</pc>
     <w lemma="speaker" pos="n1" rend="hi" xml:id="A42172-001-a-0160">Speaker</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-0170">to</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-0180">the</w>
     <w lemma="honourable" pos="j" xml:id="A42172-001-a-0190">Honourable</w>
     <w lemma="house" pos="n1" xml:id="A42172-001-a-0200">House</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-0210">of</w>
     <w lemma="commons" pos="n2" xml:id="A42172-001-a-0220">Commons</w>
     <pc xml:id="A42172-001-a-0230">,</pc>
     <w lemma="May" pos="nn1" rend="hi" xml:id="A42172-001-a-0240">May</w>
     <w lemma="3." pos="crd" xml:id="A42172-001-a-0250">3.</w>
     <w lemma="1660." pos="crd" xml:id="A42172-001-a-0260">1660.</w>
     <pc unit="sentence" xml:id="A42172-001-a-0270"/>
    </byline>
    <opener xml:id="A42172-e90">
     <salute xml:id="A42172-e100">
      <w lemma="sir" pos="n1" xml:id="A42172-001-a-0280">SIR</w>
      <w lemma="JOHN" pos="nn1" xml:id="A42172-001-a-0290">JOHN</w>
      <w lemma="Greenvile" pos="nn1" xml:id="A42172-001-a-0300">GREENVILE</w>
      <pc unit="sentence" xml:id="A42172-001-a-0310">!</pc>
     </salute>
    </opener>
    <p xml:id="A42172-e110">
     <w lemma="I" pos="pns" rend="decorinit" xml:id="A42172-001-a-0320">I</w>
     <w lemma="need" pos="vvb" xml:id="A42172-001-a-0330">Need</w>
     <w lemma="not" pos="xx" xml:id="A42172-001-a-0340">not</w>
     <w lemma="tell" pos="vvi" xml:id="A42172-001-a-0350">tell</w>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-0360">you</w>
     <w lemma="with" pos="acp" xml:id="A42172-001-a-0370">with</w>
     <w lemma="what" pos="crq" xml:id="A42172-001-a-0380">what</w>
     <w lemma="grateful" pos="j" reg="grateful" xml:id="A42172-001-a-0390">gratefull</w>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-0400">and</w>
     <w lemma="thankful" pos="j" reg="thankful" xml:id="A42172-001-a-0410">thankfull</w>
     <w lemma="heart" pos="n2" xml:id="A42172-001-a-0420">hearts</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-0430">the</w>
     <w lemma="comnoas" pos="nng1" reg="Comnoas'" xml:id="A42172-001-a-0440">Comnoas</w>
     <w lemma="now" pos="av" xml:id="A42172-001-a-0450">now</w>
     <w lemma="assemble" pos="vvn" xml:id="A42172-001-a-0460">Assembled</w>
     <w lemma="in" pos="acp" xml:id="A42172-001-a-0470">in</w>
     <w lemma="parliament" pos="n1" xml:id="A42172-001-a-0480">Parliament</w>
     <pc xml:id="A42172-001-a-0490">,</pc>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-0500">have</w>
     <w lemma="receive" pos="vvn" xml:id="A42172-001-a-0510">received</w>
     <w lemma="his" pos="po" xml:id="A42172-001-a-0520">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A42172-001-a-0530">Majesties</w>
     <w lemma="gracious" pos="j" xml:id="A42172-001-a-0540">gracious</w>
     <w lemma="letter" pos="n1" xml:id="A42172-001-a-0550">Letter</w>
     <pc xml:id="A42172-001-a-0560">,</pc>
     <foreign xml:id="A42172-e120" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0570">Res</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0580">ipsa</w>
      <w lemma="l●quitur" pos="fla" xml:id="A42172-001-a-0590">L●quitur</w>
      <pc xml:id="A42172-001-a-0600">,</pc>
     </foreign>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-0610">you</w>
     <w lemma="yourself" pos="pr" reg="yourself" xml:id="A42172-001-a-0620">your selfe</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-0640">have</w>
     <w lemma="be" pos="vvn" xml:id="A42172-001-a-0650">been</w>
     <foreign xml:id="A42172-e130" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0660">Auricularis</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0670">&amp;</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0680">Ocularis</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0690">testis</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0700">de</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0710">rei</w>
      <w lemma="n/a" pos="fla" xml:id="A42172-001-a-0720">veritate</w>
      <pc unit="sentence" xml:id="A42172-001-a-0730">.</pc>
     </foreign>
     <w lemma="our" pos="po" xml:id="A42172-001-a-0740">Our</w>
     <w lemma="bell" pos="n2" xml:id="A42172-001-a-0750">Bells</w>
     <pc xml:id="A42172-001-a-0760">,</pc>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-0770">and</w>
     <w lemma="our" pos="po" xml:id="A42172-001-a-0780">our</w>
     <w lemma="bonfire" pos="n2" xml:id="A42172-001-a-0790">Bonfires</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-0800">have</w>
     <w lemma="already" pos="av" xml:id="A42172-001-a-0810">already</w>
     <w lemma="begin" pos="vvn" xml:id="A42172-001-a-0820">begun</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-0830">the</w>
     <w lemma="proclamation" pos="n1" reg="proclamation" xml:id="A42172-001-a-0840">Proclimation</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-0850">of</w>
     <w lemma="his" pos="po" xml:id="A42172-001-a-0860">his</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A42172-001-a-0870">Majesties</w>
     <w lemma="goodness" pos="n1" reg="goodness" xml:id="A42172-001-a-0880">goodnesse</w>
     <pc xml:id="A42172-001-a-0890">,</pc>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-0900">and</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-0910">of</w>
     <w lemma="our" pos="po" xml:id="A42172-001-a-0920">our</w>
     <w lemma="joy" pos="n2" reg="joys" xml:id="A42172-001-a-0930">joyes</w>
     <pc unit="sentence" xml:id="A42172-001-a-0940">.</pc>
     <w lemma="we" pos="pns" xml:id="A42172-001-a-0950">We</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-0960">have</w>
     <w lemma="tell" pos="vvn" xml:id="A42172-001-a-0970">told</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-0980">the</w>
     <w lemma="people" pos="n1" xml:id="A42172-001-a-0990">people</w>
     <w lemma="that" pos="cs" xml:id="A42172-001-a-1000">that</w>
     <w lemma="our" pos="po" xml:id="A42172-001-a-1010">our</w>
     <w lemma="king" pos="n1" xml:id="A42172-001-a-1020">King</w>
     <pc xml:id="A42172-001-a-1030">,</pc>
     <w lemma="the" pos="d" xml:id="A42172-001-a-1040">the</w>
     <w lemma="glory" pos="n1" xml:id="A42172-001-a-1050">glory</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-1060">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A42172-001-a-1070">England</w>
     <w lemma="be" pos="vvz" xml:id="A42172-001-a-1080">is</w>
     <w lemma="come" pos="vvg" xml:id="A42172-001-a-1090">coming</w>
     <w lemma="home" pos="av-n" xml:id="A42172-001-a-1100">home</w>
     <w lemma="again" pos="av" xml:id="A42172-001-a-1110">again</w>
     <pc xml:id="A42172-001-a-1120">;</pc>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-1130">and</w>
     <w lemma="they" pos="pns" xml:id="A42172-001-a-1140">they</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-1150">have</w>
     <w lemma="resound" pos="vvn" xml:id="A42172-001-a-1160">resounded</w>
     <w lemma="it" pos="pn" xml:id="A42172-001-a-1170">it</w>
     <w lemma="back" pos="av" xml:id="A42172-001-a-1180">back</w>
     <w lemma="again" pos="av" xml:id="A42172-001-a-1190">again</w>
     <w lemma="in" pos="acp" xml:id="A42172-001-a-1200">in</w>
     <w lemma="our" pos="po" xml:id="A42172-001-a-1210">our</w>
     <w lemma="ear" pos="n2" xml:id="A42172-001-a-1220">ears</w>
     <pc xml:id="A42172-001-a-1230">,</pc>
     <w lemma="that" pos="cs" xml:id="A42172-001-a-1240">that</w>
     <w lemma="they" pos="pns" xml:id="A42172-001-a-1250">they</w>
     <w lemma="be" pos="vvb" xml:id="A42172-001-a-1260">are</w>
     <w lemma="ready" pos="j" xml:id="A42172-001-a-1270">ready</w>
     <pc xml:id="A42172-001-a-1280">,</pc>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-1290">and</w>
     <w lemma="their" pos="po" xml:id="A42172-001-a-1300">their</w>
     <w lemma="heart" pos="n2" xml:id="A42172-001-a-1310">hearts</w>
     <w lemma="be" pos="vvb" xml:id="A42172-001-a-1320">are</w>
     <w lemma="open" pos="j" xml:id="A42172-001-a-1330">open</w>
     <w lemma="to" pos="prt" xml:id="A42172-001-a-1340">to</w>
     <w lemma="receive" pos="vvi" xml:id="A42172-001-a-1350">receive</w>
     <w lemma="he" pos="pno" xml:id="A42172-001-a-1360">him</w>
     <pc unit="sentence" xml:id="A42172-001-a-1370">.</pc>
     <w lemma="both" pos="av-d" xml:id="A42172-001-a-1380">Both</w>
     <w lemma="parliament" pos="n1" xml:id="A42172-001-a-1390">Parliament</w>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-1400">and</w>
     <w lemma="people" pos="n1" xml:id="A42172-001-a-1410">people</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-1420">have</w>
     <w lemma="cry" pos="vvn" reg="cried" xml:id="A42172-001-a-1430">cryed</w>
     <w lemma="aloud" pos="av" xml:id="A42172-001-a-1440">aloud</w>
     <w lemma="in" pos="acp" xml:id="A42172-001-a-1450">in</w>
     <w lemma="their" pos="po" xml:id="A42172-001-a-1460">their</w>
     <w lemma="prayer" pos="n1" xml:id="A42172-001-a-1470">prayer</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-1480">to</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-1490">the</w>
     <w lemma="king" pos="n1" xml:id="A42172-001-a-1500">King</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-1510">of</w>
     <w lemma="king" pos="n2" xml:id="A42172-001-a-1520">Kings</w>
     <pc xml:id="A42172-001-a-1530">,</pc>
     <hi xml:id="A42172-e150">
      <w lemma="long" pos="av-j" xml:id="A42172-001-a-1540">Long</w>
      <w lemma="live" pos="vvb" xml:id="A42172-001-a-1550">live</w>
      <w lemma="king" pos="n1" xml:id="A42172-001-a-1560">King</w>
     </hi>
     <w lemma="Charles" pos="nn1" xml:id="A42172-001-a-1570">Charles</w>
     <hi xml:id="A42172-e160">
      <w lemma="the" pos="d" xml:id="A42172-001-a-1580">the</w>
      <w lemma="second" pos="ord" xml:id="A42172-001-a-1590">2d</w>
      <pc unit="sentence" xml:id="A42172-001-a-1600">.</pc>
     </hi>
    </p>
    <p xml:id="A42172-e170">
     <w lemma="I" pos="pns" xml:id="A42172-001-a-1610">I</w>
     <w lemma="be" pos="vvm" xml:id="A42172-001-a-1620">am</w>
     <w lemma="likewise" pos="av" xml:id="A42172-001-a-1630">likewise</w>
     <w lemma="to" pos="prt" xml:id="A42172-001-a-1640">to</w>
     <w lemma="tell" pos="vvi" xml:id="A42172-001-a-1650">tell</w>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-1660">you</w>
     <pc xml:id="A42172-001-a-1670">,</pc>
     <w lemma="that" pos="cs" xml:id="A42172-001-a-1680">that</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-1690">the</w>
     <w lemma="house" pos="n1" xml:id="A42172-001-a-1700">House</w>
     <w lemma="do" pos="vvz" xml:id="A42172-001-a-1710">doth</w>
     <w lemma="not" pos="xx" xml:id="A42172-001-a-1720">not</w>
     <w lemma="think" pos="vvi" xml:id="A42172-001-a-1730">think</w>
     <w lemma="fit" pos="j" xml:id="A42172-001-a-1740">fit</w>
     <w lemma="that" pos="cs" xml:id="A42172-001-a-1750">that</w>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-1760">you</w>
     <w lemma="shall" pos="vmd" xml:id="A42172-001-a-1770">should</w>
     <w lemma="return" pos="vvi" xml:id="A42172-001-a-1780">return</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-1790">to</w>
     <w lemma="our" pos="po" xml:id="A42172-001-a-1800">our</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="A42172-001-a-1810">Royall</w>
     <w lemma="sovereign" pos="n1-j" reg="sovereign" xml:id="A42172-001-a-1820">Soveraign</w>
     <w lemma="without" pos="acp" xml:id="A42172-001-a-1830">without</w>
     <w lemma="some" pos="d" xml:id="A42172-001-a-1840">some</w>
     <w lemma="testimony" pos="n1" xml:id="A42172-001-a-1850">Testimony</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-1860">of</w>
     <w lemma="their" pos="po" xml:id="A42172-001-a-1870">their</w>
     <w lemma="respect" pos="n2" xml:id="A42172-001-a-1880">respects</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-1890">to</w>
     <w lemma="yourself" pos="pr" reg="yourself" xml:id="A42172-001-a-1900">your self</w>
     <pc xml:id="A42172-001-a-1920">;</pc>
     <w lemma="they" pos="pns" xml:id="A42172-001-a-1930">They</w>
     <w lemma="have" pos="vvb" xml:id="A42172-001-a-1940">have</w>
     <w lemma="order" pos="vvn" xml:id="A42172-001-a-1950">ordered</w>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-1960">and</w>
     <w lemma="appoint" pos="vvn" xml:id="A42172-001-a-1970">appointed</w>
     <w lemma="that" pos="d" xml:id="A42172-001-a-1980">that</w>
     <w lemma="five" pos="crd" xml:id="A42172-001-a-1990">five</w>
     <w lemma="hundred" pos="crd" xml:id="A42172-001-a-2000">hundred</w>
     <w lemma="pound" pos="n1" xml:id="A42172-001-a-2010">pound</w>
     <w lemma="shall" pos="vmb" xml:id="A42172-001-a-2020">shall</w>
     <w lemma="be" pos="vvi" xml:id="A42172-001-a-2030">be</w>
     <w lemma="deliver" pos="vvn" xml:id="A42172-001-a-2040">delivered</w>
     <w lemma="unto" pos="acp" xml:id="A42172-001-a-2050">unto</w>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-2060">you</w>
     <w lemma="to" pos="prt" xml:id="A42172-001-a-2070">to</w>
     <w lemma="buy" pos="vvi" xml:id="A42172-001-a-2080">buy</w>
     <w lemma="a" pos="d" xml:id="A42172-001-a-2090">a</w>
     <w lemma="jewel" pos="n1" reg="jewel" xml:id="A42172-001-a-2100">Jewell</w>
     <pc xml:id="A42172-001-a-2110">,</pc>
     <w lemma="as" pos="acp" xml:id="A42172-001-a-2120">as</w>
     <w lemma="a" pos="d" xml:id="A42172-001-a-2130">a</w>
     <w lemma="badge" pos="n1" xml:id="A42172-001-a-2140">badge</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-2150">of</w>
     <w lemma="that" pos="d" xml:id="A42172-001-a-2160">that</w>
     <w lemma="honour" pos="n1" xml:id="A42172-001-a-2170">honour</w>
     <w lemma="which" pos="crq" xml:id="A42172-001-a-2180">which</w>
     <w lemma="be" pos="vvz" xml:id="A42172-001-a-2190">is</w>
     <w lemma="due" pos="j" xml:id="A42172-001-a-2200">due</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-2210">to</w>
     <w lemma="a" pos="d" xml:id="A42172-001-a-2220">a</w>
     <w lemma="person" pos="n1" xml:id="A42172-001-a-2230">person</w>
     <w lemma="who" pos="crq" xml:id="A42172-001-a-2240">whom</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-2250">the</w>
     <w lemma="king" pos="n1" xml:id="A42172-001-a-2260">King</w>
     <w lemma="have" pos="vvz" xml:id="A42172-001-a-2270">hath</w>
     <w lemma="honour" pos="vvn" xml:id="A42172-001-a-2280">honoured</w>
     <w lemma="to" pos="prt" xml:id="A42172-001-a-2290">to</w>
     <w lemma="be" pos="vvi" xml:id="A42172-001-a-2310">be</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-2320">the</w>
     <w lemma="messenger" pos="n1" xml:id="A42172-001-a-2330">Messenger</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-2340">of</w>
     <w lemma="so" pos="av" xml:id="A42172-001-a-2350">so</w>
     <w lemma="gracious" pos="j" reg="gracious" xml:id="A42172-001-a-2360">gratious</w>
     <w lemma="a" pos="d" xml:id="A42172-001-a-2370">a</w>
     <w lemma="message" pos="n1" xml:id="A42172-001-a-2380">message</w>
     <pc unit="sentence" xml:id="A42172-001-a-2390">.</pc>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-2400">And</w>
     <w lemma="I" pos="pns" xml:id="A42172-001-a-2410">I</w>
     <w lemma="be" pos="vvm" xml:id="A42172-001-a-2420">am</w>
     <w lemma="command" pos="vvn" xml:id="A42172-001-a-2430">commanded</w>
     <w lemma="in" pos="acp" xml:id="A42172-001-a-2440">in</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-2450">the</w>
     <w lemma="name" pos="n1" xml:id="A42172-001-a-2460">Name</w>
     <w lemma="of" pos="acp" xml:id="A42172-001-a-2470">of</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-2480">the</w>
     <w lemma="house" pos="n1" xml:id="A42172-001-a-2490">House</w>
     <w lemma="to" pos="prt" xml:id="A42172-001-a-2500">to</w>
     <w lemma="return" pos="vvi" xml:id="A42172-001-a-2510">return</w>
     <w lemma="to" pos="acp" xml:id="A42172-001-a-2520">to</w>
     <w lemma="you" pos="pn" xml:id="A42172-001-a-2530">you</w>
     <w lemma="their" pos="po" xml:id="A42172-001-a-2540">their</w>
     <w lemma="very" pos="j" xml:id="A42172-001-a-2550">very</w>
     <w lemma="hearty" pos="j" xml:id="A42172-001-a-2560">hearty</w>
     <w lemma="thanks" pos="n2" xml:id="A42172-001-a-2570">thanks</w>
     <pc unit="sentence" xml:id="A42172-001-a-2580">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A42172-e180">
   <div type="colophon" xml:id="A42172-e190">
    <p xml:id="A42172-e200">
     <hi xml:id="A42172-e210">
      <w lemma="london" pos="nn1" xml:id="A42172-001-a-2590">London</w>
      <pc xml:id="A42172-001-a-2600">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A42172-001-a-2610">Printed</w>
     <w lemma="by" pos="acp" xml:id="A42172-001-a-2620">by</w>
     <w lemma="h." pos="ab" rend="hi" xml:id="A42172-001-a-2630">H.</w>
     <w lemma="b." pos="ab" xml:id="A42172-001-a-2640">B.</w>
     <w lemma="and" pos="cc" xml:id="A42172-001-a-2650">and</w>
     <w lemma="be" pos="vvb" xml:id="A42172-001-a-2660">are</w>
     <w lemma="sell" pos="vvn" xml:id="A42172-001-a-2670">sold</w>
     <w lemma="at" pos="acp" xml:id="A42172-001-a-2680">at</w>
     <w lemma="the" pos="d" xml:id="A42172-001-a-2690">the</w>
     <w lemma="George" pos="nn1" rend="hi" xml:id="A42172-001-a-2700">George</w>
     <w lemma="in" pos="acp" xml:id="A42172-001-a-2710">in</w>
     <hi xml:id="A42172-e240">
      <w lemma="Holbourn" pos="nn1" xml:id="A42172-001-a-2720">Holbourn</w>
      <pc unit="sentence" xml:id="A42172-001-a-2730">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
